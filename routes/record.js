const express = require("express");
const multer = require('multer');
const mongoose = require('mongoose');
const path = require('path');
const cors = require('cors');


// recordRoutes is an instance of the express router.
// We use it to define our routes.
// The router will be added as a middleware and will take control of requests starting with path /record.
const recordRoutes = express.Router();
const app = express();
app.use(cors());
app.use(express.json());
app.use(express.static('public'));

// This will help us connect to the database
const dbo = require("../db/conn");

// This help convert the id from string to ObjectId for the _id.
const ObjectId = require("mongodb").ObjectId;

// Multer configuration
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'public/files/');
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + path.extname(file.originalname));
  },
});
const upload = multer({ storage });


// Define a schema for images
const imageSchema = new mongoose.Schema({
  filename: String,
  path: String,
});

const Image = mongoose.model('Image', imageSchema);

// Route to handle image uploads
recordRoutes.post('/upload', upload.single('image'), async (req, res) => {
  try {
    // const { filename, path } = req.file;
    // const newImage = new Image({ filename, path });
    // await newImage.save();
    console.log('[test] req', req.file)
    res.json({ message: 'Image uploaded successfully', name: req.file.filename });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Failed to upload image' });
  }
});


// This section will help you get a list of all the records.
recordRoutes.route("/records").get(function (req, res) {
  let db_connect = dbo.getDb();
  db_connect
    .collection("records")
    .find({})
    .toArray(function (err, result) {
      if (err) throw err;
      res.json(result);
    });
});


// This section will help you create a new record.
recordRoutes.route("/record/add").post(function (req, response) {
  let db_connect = dbo.getDb();
  let myobj = {
    name: "hey",
  };
  db_connect.collection("records").insertOne(myobj, function (err, res) {
    if (err) throw err;
    response.json(res);
  });
});


module.exports = recordRoutes;
